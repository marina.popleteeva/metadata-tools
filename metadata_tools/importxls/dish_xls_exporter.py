import logging
import pyexcel
import hashlib
from os import fsencode
from .export_utils import (
    validate_dataset_dict,
    get_partners_from_daisy,
    get_email_from_string,
    process_possible_date,
    process_yes_no_dontknow_na_answer,
    clean_sheet,
    get_value_list_from_row,
    is_data,
    is_study,
    is_submission,
    process_yes_no_answer,
    get_names_from_string,
    has_value,
)
from .validators import DISHVersionValidator


class DishXlsExporter:
    def __init__(self):
        logging.basicConfig(
            filename="export_dishxls.log",
            level=logging.DEBUG,
            format="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        institutions = get_partners_from_daisy()
        self.inst_dict = {}
        self.inst_ac_dict = {}
        for inst in institutions:
            self.inst_dict[inst.get("name").lower()] = inst.get("name")
        for inst in institutions:
            if inst.get("acronym"):
                self.inst_ac_dict[inst.get("acronym").lower()] = inst.get("name")

        self.h = hashlib.md5()
        self.predefined_data_types = set(
            [
                "Omics data",
                "Genotype data",
                "Whole genome sequencing",
                "Exome sequencing",
                "Genomics variant array",
                "RNASeq",
                "Genetic and derived genetic data",
                "Transcriptome array",
                "Methylation array",
                "MicroRNA array",
                "Metabolomics",
                "Metagenomics",
                "Proteomics",
                "Other omics data",
                "Clinical Imaging",
                "Cell Imaging",
                "Human subject data",
                "Clinical data",
                "Lifestyle data",
                "Socio Economic Data",
                "Environmental Data",
                "Other Phenotype data",
                "Other",
            ]
        )

    def export_submission(self, full_file_path, skip_version_validation=False):
        idx = 1
        logging.info("Processing start for ----> {}".format(full_file_path))
        book = pyexcel.get_book(file_name=full_file_path, skip_hidden_sheets=False)
        is_dish = any("_Help" in elem for elem in book.sheet_names())

        if not skip_version_validation:
            version_validator = DISHVersionValidator()
            version_validator.validate_book(book)

        if is_dish:
            dataset_dict = {
                "source": book.filename,
                "contacts": [],
                "data_declarations": [],
                "studies": [],
                "legal_bases": [],
            }

            while idx < book.number_of_sheets():
                sheet = book.sheet_by_index(idx)
                logging.info(
                    "Processing sheet ----> {}".format(book.sheet_names()[idx])
                )
                sheet = clean_sheet(sheet)

                if is_study(sheet):
                    cohort_dict = self.process_study_sheet(sheet)
                    dataset_dict["studies"].append(cohort_dict)

                elif is_data(sheet):
                    dataset_dict = self.process_dataset_sheet(dataset_dict, sheet)

                elif is_submission(sheet):
                    dataset_dict["name"] = sheet[2, 1].strip()
                    dataset_dict["project"] = sheet[5, 1].strip()
                    dataset_dict["contacts"].extend(self.get_submission_contacts(sheet))
                else:
                    pass

                idx += 1
            validate_dataset_dict(dataset_dict)
            logging.info("Processing end for ----> {}".format(full_file_path))
            return dataset_dict
        else:
            raise ValueError("{} not a valid DISH Excel file".format(full_file_path))

    def process_dataset_sheet(self, dataset_dict, sheet):
        datadec_dict = {
            "title": sheet[1, 1].strip(),
            "source_study": sheet[2, 1].strip(),
            "data_types": [],
        }
        datadec_dict["data_type_notes"] = sheet[7, 1]

        data_type_info = self.process_data_types(get_value_list_from_row(sheet, 6))
        datadec_dict["data_types"].extend(data_type_info[0])
        if data_type_info[1]:
            datadec_dict["data_type_notes"] += " " + data_type_info[1]
        if sheet[10, 1]:
            datadec_dict["data_type_notes"] += " Notes on samples: " + sheet[10, 1]

        # if it involves samples add this as a datatype
        if process_yes_no_answer(sheet[9, 1]):
            datadec_dict["data_types"].append("Samples")

        if has_value(sheet[8, 1]):
            datadec_dict["de_identification"] = self.process_deidentification(
                sheet[8, 1]
            )
        datadec_dict["consent_status"] = self.process_consent_status(sheet[32, 1])
        datadec_dict["consent_status_description"] = sheet[33, 1]

        if sheet[20, 1]:
            datadec_dict["subjects_category"] = sheet[20, 1].replace(" & ", "_and_")

        if has_value(sheet[12, 1]):
            lb_code = self.extract_lb_code(sheet[12, 1])
            dataset_dict["legal_bases"].append(
                {
                    "data_declarations": [sheet[1, 1]],
                    "personal_data_codes": ["Standard"],
                    "legal_basis_codes": [lb_code],
                    "legal_basis_notes": sheet[12, 0],
                }
            )

        if sheet[13, 1]:
            lb_code = self.extract_lb_code(sheet[13, 1])
            dataset_dict["legal_bases"].append(
                {
                    "data_declarations": [sheet[1, 1]],
                    "personal_data_codes": ["Standard"],
                    "legal_basis_codes": [lb_code],
                    "legal_basis_notes": sheet[13, 0],
                }
            )
        if has_value(sheet[16, 1]):
            lb_code = self.extract_lb_code(sheet[16, 1])
            dataset_dict["legal_bases"].append(
                {
                    "data_declarations": [sheet[1, 1]],
                    "personal_data_codes": ["Special"],
                    "legal_basis_codes": [lb_code],
                    "legal_basis_notes": sheet[16, 0],
                }
            )
        if has_value(sheet[17, 1]):
            lb_code = self.extract_lb_code(sheet[17, 1])
            dataset_dict["legal_bases"].append(
                {
                    "data_declarations": [sheet[1, 1]],
                    "personal_data_codes": ["Special"],
                    "legal_basis_codes": [lb_code],
                    "legal_basis_notes": sheet[17, 0],
                }
            )
        if has_value(sheet[21, 1]):
            datadec_dict["has_special_subjects"] = process_yes_no_dontknow_na_answer(
                sheet[21, 1]
            )
            if datadec_dict.get("has_special_subjects") == True and sheet[22, 1]:
                datadec_dict["special_subject_notes"] = sheet[22, 1]

        use_restrictions = []
        # Is data consented to be used only in specific research/disease areas?
        if process_yes_no_dontknow_na_answer(sheet[24, 1]):
            use_restrictions.append(
                {
                    "use_class": "RS",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[24, 0],
                    "use_restriction_note": sheet[25, 1],
                }
            )
        elif process_yes_no_dontknow_na_answer(sheet[24, 1]) is not None:
            use_restrictions.append(
                {
                    "use_class": "RS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[24, 0],
                    "use_restriction_note": "Use is not limited to a specific research/disease area.",
                }
            )

        # Does consent contain clauses that put geographical restrictions to the sharing of data?
        if process_yes_no_dontknow_na_answer(sheet[26, 1]):
            use_restrictions.append(
                {
                    "use_class": "GS",
                    "use_restriction_rule": "PROHIBITION",
                    "use_class_note": sheet[26, 0],
                    "use_restriction_note": "Consent does not limit the type of recipients.",
                }
            )
        elif process_yes_no_dontknow_na_answer(sheet[26, 1]) is not None:
            use_restrictions.append(
                {
                    "use_class": "GS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[26, 0],
                    "use_restriction_note": "Consent does not contain clauses of geographical restrictions.",
                }
            )

        # Does the consent limit the type of recipients?
        if process_yes_no_dontknow_na_answer(sheet[28, 1]):
            use_restrictions.append(
                {
                    "use_class": "IS",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[28, 0],
                    "use_restriction_note": sheet[29, 1],
                }
            )
        elif process_yes_no_dontknow_na_answer(sheet[28, 1]) is not None:
            use_restrictions.append(
                {
                    "use_class": "IS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[28, 0],
                    "use_restriction_note": sheet[28, 1],
                }
            )

        # Does the consent contain clauses that put time-limits on the use of data?
        if process_yes_no_dontknow_na_answer(sheet[30, 1]):
            use_restrictions.append(
                {
                    "use_class": "TS",
                    "use_restriction_rule": "OBLIGATION",
                    "use_class_note": sheet[30, 0],
                    "use_restriction_note": sheet[31, 1],
                }
            )
        elif process_yes_no_dontknow_na_answer(sheet[30, 1]) is not None:
            use_restrictions.append(
                {
                    "use_class": "TS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[30, 0],
                    "use_restriction_note": "Consent does not put time-limits on data use.",
                }
            )

        # Is the use of data limited to the project named in the Submission sheet?
        if process_yes_no_answer(sheet[35, 1]):
            use_restrictions.append(
                {
                    "use_class": "PS",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[35, 0],
                    "use_restriction_note": dataset_dict["project"],
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "PS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[35, 0],
                }
            )

        # Does the limitation to the RESEARCH PROJECT include the Research Use (as defined in the Consortium Agreement)?
        if process_yes_no_answer(sheet[36, 1]):
            use_restrictions.append(
                {
                    "use_class": "PS",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[36, 0],
                    "use_restriction_note": "Limitation to the reseearch project includes the Reserach Use.",
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "PS",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[36, 0],
                }
            )

        # Is the data being sent to ELIXIR-LU/LCSB for a limited duration?
        if process_yes_no_answer(sheet[38, 1]):
            retention_date = process_possible_date(sheet[39, 1])
            if retention_date:
                datadec_dict["storage_end_date"] = retention_date
                datadec_dict["storage_duration_criteria"] = 'Retention period defined by DISH'
            else:
                datadec_dict["storage_duration_criteria"] = sheet[39, 1]
        else:
            datadec_dict["storage_duration_criteria"] = "DISH specified no restriction on retention."

        # Are there any requirements in case of publications based on the DATA?
        if process_yes_no_answer(sheet[40, 1]):
            use_restrictions.append(
                {
                    "use_class": "PUB",
                    "use_restriction_rule": "OBLIGATION",
                    "use_class_note": sheet[40, 0],
                    "use_restriction_note": sheet[41, 1],
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "PUB",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[40, 0],
                }
            )

        # Is there a requirement to return data or documents to the database/resource?
        if process_yes_no_answer(sheet[42, 1]):
            use_restrictions.append(
                {
                    "use_class": "RTN",
                    "use_restriction_rule": "OBLIGATION",
                    "use_class_note": sheet[42, 0],
                    "use_restriction_note": sheet[43, 1],
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "RTN",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[42, 0],
                    "use_restriction_note": "There is no requirement to return data.",
                }
            )

        # Is the use limited to approved users/groups/institutions?
        if process_yes_no_answer(sheet[44, 1]):
            use_restrictions.append(
                {
                    "use_class": "US",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[44, 0],
                    "use_restriction_note": sheet[45, 1],
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "US",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[44, 1],
                }
            )

        # If there are any other restrictions on DATA, please describe them here. If applicable, in your description you may refer to GA4GH Data Use Category Codes, found at the below link.
        if sheet[46, 1]:
            use_restrictions.append(
                {
                    "use_class": "Other",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": "Other restrictions",
                    "use_restriction_note": sheet[46, 1],
                }
            )

        datadec_dict["access_procedure"] = ""
        if sheet[48, 1] and ("not" in sheet[49, 1]):  #
            if ("No" in sheet[48, 1]) and sheet[49, 1]:
                datadec_dict[
                    "access_category"
                ] = "open_access"  # this is just an initial interpretation and should be further curated in catalog
                datadec_dict["access_procedure"] = (
                    datadec_dict["access_procedure"]
                    + "No additional form is needed to request access."
                    + sheet[49, 1]
                )
            else:
                datadec_dict[
                    "access_category"
                ] = "registered_access"  # this is just an initial interpretation and should be further curated in catalog
                datadec_dict["access_procedure"] = (
                    datadec_dict["access_procedure"]
                    + "Additional form is needed to request access."
                    + sheet[49, 1]
                )
        else:
            datadec_dict["access_category"] = "controlled_access"
            datadec_dict["access_procedure"] = (
                datadec_dict["access_procedure"] + sheet[49, 1] + sheet[50, 1]
            )

        # Are there any IP restrictions/requirements when using the DATA?
        if process_yes_no_answer(sheet[51, 1]):
            use_restrictions.append(
                {
                    "use_class": "IP",
                    "use_restriction_rule": "CONSTRAINED_PERMISSION",
                    "use_class_note": sheet[51, 0],
                    "use_restriction_note": sheet[52, 1],
                }
            )
        else:
            use_restrictions.append(
                {
                    "use_class": "IP",
                    "use_restriction_rule": "PERMISSION",
                    "use_class_note": sheet[51, 0],
                    "use_restriction_note": sheet[51, 1],
                }
            )
        datadec_dict["use_restrictions"] = use_restrictions

        dataset_dict["data_declarations"].append(datadec_dict)

        return dataset_dict

    def process_study_sheet(self, sheet):
        cohort_dict = {
            "name": sheet[1, 1].strip(),
            "description": sheet[2, 1] + " " + sheet[6, 1] + " " + sheet[16, 1],
            "has_ethics_approval": process_yes_no_answer(sheet[4, 1]),
            "ethics_approval_notes": sheet[5, 1],
            "url": sheet[3, 1],
            "contacts": [
                {
                    "first_name": get_names_from_string(sheet[8, 1])[0],
                    "last_name": get_names_from_string(sheet[8, 1])[1],
                    "role": sheet[11, 1],
                    "email": get_email_from_string(sheet[9, 1]),
                    "affiliations": [self.process_institution(sheet[10, 1])],
                }
            ],
        }

        if sheet[12, 1] and sheet[15, 1]:
            cohort_dict["contacts"].append(
                {
                    "first_name": get_names_from_string(sheet[12, 1])[0],
                    "last_name": get_names_from_string(sheet[12, 1])[1],
                    "role": sheet[15, 1],
                    "email": get_email_from_string(sheet[13, 1]),
                    "affiliations": [self.process_institution(sheet[14, 1])],
                }
            )
        if sheet[16, 1]:
            cohort_dict["description"] = (
                cohort_dict["description"] + " Remarks: " + sheet[16, 1]
            )

        return cohort_dict

    def get_submission_contacts(self, sheet):
        contacts = []
        affiliations = [self.process_institution(sheet[7, 1])]
        if sheet[9, 1]:
            contacts.append(
                {
                    "first_name": get_names_from_string(sheet[9, 1])[0],
                    "last_name": get_names_from_string(sheet[9, 1])[1],
                    "role": sheet[11, 1],
                    "email": get_email_from_string(sheet[10, 1]),
                    "affiliations": affiliations,
                }
            )
        else:
            logging.error("Submission contact missing - Main contact!")

        if sheet[12, 1]:
            contacts.append(
                {
                    "first_name": get_names_from_string(sheet[12, 1])[0],
                    "last_name": get_names_from_string(sheet[12, 1])[1],
                    "role": "Legal_Representative",
                    "email": sheet[13, 1].strip(),
                    "affiliations": affiliations,
                }
            )
        else:
            logging.error("Submission contact missing - Legal Representative!")

        if sheet[14, 1]:
            contacts.append(
                {
                    "first_name": get_names_from_string(sheet[14, 1])[0],
                    "last_name": get_names_from_string(sheet[14, 1])[1],
                    "role": "Data_Protection_Officer",
                    "email": get_email_from_string(sheet[15, 1]),
                    "affiliations": affiliations,
                }
            )
        else:
            logging.error("Submission contact missing - DPO!")

        if sheet[16, 1] and sheet[18, 1]:
            contacts.append(
                {
                    "first_name": get_names_from_string(sheet[16, 1])[0],
                    "last_name": get_names_from_string(sheet[16, 1])[1],
                    "role": sheet[18, 1],
                    "email": get_email_from_string(sheet[17, 1]),
                    "affiliations": affiliations,
                }
            )

        return contacts

    def get_hash_for_path(self, path):
        self.h.update(fsencode(path))
        return str(int(self.h.hexdigest(), 16))

    def process_data_types(self, xls_data_type_list):
        result = []
        data_type_notes = ""
        for type_name in xls_data_type_list:
            type_name = type_name.strip()
            if type_name:
                if type_name in self.predefined_data_types:
                    result.append(type_name.replace(" ", "_"))
                else:
                    data_type_notes += type_name + "\n"
        return (result, data_type_notes)

    def process_deidentification(self, deid_str):
        if "seu" in deid_str:
            return "pseudonymization"
        elif "non" in deid_str:
            return "anonymization"

    def process_consent_status(self, consent_str):
        if "et" in consent_str:
            return "heterogeneous"
        elif "om" in consent_str:
            return "homogeneous"
        else:
            return None

    def process_institution(self, institution_str):
        if institution_str:
            if self.inst_ac_dict.get(institution_str.lower()):
                return self.inst_ac_dict.get(institution_str.lower())
            elif self.inst_dict.get(institution_str.lower()):
                return self.inst_dict.get(institution_str.lower())
            else:
                logging.error("Unknown institution  -- > {}".format(institution_str))
                return institution_str
        else:
            return ""

    def extract_lb_code(self, lb_value):
        op = lb_value.index("(")
        cp = lb_value.rindex(")")
        return lb_value[op + 1 : cp]
