import argparse
import os
import pathlib
from metadata_tools.importxls.dish_xls_exporter import DishXlsExporter
from metadata_tools.importxls.export_utils import save_exported_datasets_to_file


parser = argparse.ArgumentParser(description='Load XLS file and export as JSON')

parser.add_argument(
    '-f',
    '--file',
    nargs='+',
    help='Path to XLS/XLSX file to parse',
    default=False)

parser.add_argument(
    '-d',
    '--directory',
    help='Directory with XLS/XLSX files',
    default=False,
    dest='dirname')

parser.add_argument(
    '-o',
    '--output-dir',
    help='Path to the directory to store JSON files to.',
    required=True,
    dest="output_dir"
)

parser.add_argument(
    '--skip-version-validation',
    help='',
    dest="skip_version_validation", 
    action='store_true'
)


args = parser.parse_args()
if not (args.dirname or args.file):
    parser.error('No arguments provided. Either -d or -f must be specified')

fileList = []

if args.dirname:
    dirname = args.dirname
    for f in os.listdir(dirname):
        filename = os.path.join(dirname, f)
        if os.path.isfile(filename):
            fileList.append(filename)

if args.file:
    for f in args.file:
        if os.path.isfile(f):
            fileList.append(f)
        else:
            print(f"'{f}' is not file - skipping")


exporter = DishXlsExporter()
counter = 0
for fname in fileList:
    try:
        dataset_dict = exporter.export_submission(fname, args.skip_version_validation)
    except Exception as e:
        raise Exception(f'Error occured in file {fname}') from e

    fname_json = pathlib.Path(fname).stem + ".json"
    fname_out = os.path.join(args.output_dir, fname_json)
    with open(fname_out, 'w', encoding='utf-8') as outfile:
        save_exported_datasets_to_file(dataset_dict, outfile)
    counter += 1

print(f'File export successful: {counter} files exported to {args.output_dir}')
