#!/usr/bin/env python3
# -*- coding: utf-8 -*-



try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

requirements = [
    'jsonschema',
    'pyexcel==0.7.0',
    'pyexcel-xls',
    'pyexcel-xlsx==0.6.0',
    'openpyxl==3.0.10'
]

test_requirements = [
    'coverage'
]

setup(
    name='metadata-tools',
    version='0.2.0-dev',
    description="Set of utilities to validate and generate data information files that conform to the ELIXIR LU model.",
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    package_dir={'metadata_tools': 'metadata_tools'},
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    keywords=['elixir', 'metadata'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        # 'Intended Audience :: Developers',
        # 'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.8',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    package_data={
        'metadata-tools': ['metadata_tools/resources/*']
    },
    extras_require={
        'dev': [
            'tox',
            'pep8',
            'bumpversion',
            'coverage'
        ]
    }
)
