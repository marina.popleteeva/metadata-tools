# Elixir LU metadata utilities

## Clone the repository

Do not forget to also **clone the submodules!**

```bash
git clone <repository-url>
git submodule update --init
```

## Versions of DISH
Tags are used for keeping track of which code version supported which DISH version.

 Old DISH exporter can produce JSON which is not compatible with newest JSON schemas or Daisy importer. In this case, migration of the DISH is the safest procedure.

 Versions:
 - v.8 -> v.9 - no schema breaking change, update of one text field
 - v.9 -> v.10 - no schema breaking change, just more data use restrictions are collected
 - v.10 -> v.11 - schema v0.0.6 is used, some questions have now N/A option and one question related to legal background is added

## Development environment setup

In order to pull partners from one of the DAISY instances, specify its URL in `settings.py` file.

Create virtual environment:

```bash
mkdir project_venv
python3 -m venv project_venv
source ./project_venv/bin/activate
```

Install dependencies (single brackets are required if you are using `zsh`):

```bash
pip install -e '.[dev]'
```

## Testing

Run tests with:

```bash
python setup.py test
```

## Convert Excel DISH file to JSON file

```bash
python metadata_tools/commands/export_xls_to_json.py -o <OUTPUT-DIR> -f <PATH-TO-EXCEL-FILE-TO-CONVERT>
```
## Current Version

**v0.2.0-dev**
